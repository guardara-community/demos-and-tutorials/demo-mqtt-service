FROM ubuntu:20.04 as builder

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get -y update
RUN apt-get -y upgrade
RUN apt-get -y install libffi-dev git gcc openssl libssl-dev zlib1g-dev build-essential cmake

RUN mkdir /builder
COPY ./src/nanomq /builder/

RUN mkdir /builder/build/
WORKDIR /builder/build/
RUN cd /builder/build/
RUN cmake .. 
RUN make

FROM ubuntu:20.04

ENV DEBIAN_FRONTEND noninteractive
ENV SERVICE_HOME /demo
ENV SERVICE_USER nobody
ENV SERVICE_GROUP nogroup
WORKDIR ${SERVICE_HOME}

RUN apt-get -y update
RUN apt-get -y upgrade
RUN apt-get -y install gdb gdbserver

COPY --from=builder /builder/build/nanomq/nanomq ./
RUN mkdir -p ${SERVICE_HOME}/etc
COPY --from=builder /builder/etc/* /etc/
COPY --from=builder /builder/etc/* ${SERVICE_HOME}/etc/

RUN chown -R ${SERVICE_USER}:${SERVICE_GROUP} ${SERVICE_HOME}/nanomq
RUN chmod 700 ${SERVICE_HOME}/nanomq

EXPOSE 19999
EXPOSE 1883
USER ${SERVICE_USER}:${SERVICE_GROUP}
CMD gdbserver --multi 0.0.0.0:19999
